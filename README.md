# xboxdrv-converter

tool to help into the convertion of gamepad into xbox360


Connect your controller via usb or bluetooth run the script, and it will automaticly connecte your controller with your config file simply by launching this command :
```
xboxdrv-converter
```

First, you have to find your controller, and event names :
```
cat /proc/bus/input/devices | sed -re '/PLAYSTATION\(R\)3 Controller\"|js|X-Box|Xbox|SHANWAN PS3 GamePad Motion Sensors/!d' | sed -e ':a' -e 'N' -e '$!ba' -e 's/\"\nH: Handlers=/\" /g' | sed 's/N: Name=//g ; /^H:/d ; s/js[0-9]*//g ; s/kbd //g '
```
should result on something like this :
```
"Sony PLAYSTATION(R)3 Controller" event5  
"Xbox Wireless Controller" event22 
```
## Now you can create your config file :

```
evtest /dev/input/eventX
```


Raw controller inputs on the left map to the xbox equivalent on the right in the file who have the same name of your gamepad
like this 
```
Sony PLAYSTATION(R)3 Controller.xboxdrv
Xbox Wireless Controller.xboxdrv
```
```
[axismap]
-Y1 = Y1
-Y2 = Y2

[evdev-absmap]
ABS_X  = X1
ABS_Y  = Y1
ABS_Z  = X2
ABS_RZ = Y2
ABS_HAT0X = DPAD_X
ABS_HAT0Y = DPAD_Y

[evdev-keymap]
BTN_B = A
BTN_C = B
BTN_A = X
BTN_X = Y
BTN_Y = LB
BTN_Z = RB
BTN_TL = LT
BTN_TR = RT
BTN_SELECT = TL
BTN_START = TR
BTN_TL2 = BACK
BTN_TR2 = START
BTN_MODE = GUIDE
```
![](https://gitlab.com/PowaBanga/xboxdrv-converter/-/raw/master/Xbox_Controller.png)

# P.S.

The script store pid of xdoxdrv in xboxdrv.running file
you can do this to kill xboxdrv
```
kill -9 $(cat $PWD/xboxdrv.running)
```
## Dependencies
xboxdrv

sed

cat


# License

By submitting code to this repository, you agree to distribute it under a GPL-compatible license. All files shall be GPLv3 licensed, unless specified otherwise.
